package test.Package;

import android.app.Activity;
import android.os.Bundle;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.team6.nick.telecorp.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class testerActivity extends Activity {

    ParseObject testObj;
    JSONArray testServiceA;
    JSONArray testServiceB;
    JSONArray serviceArray = new JSONArray();
    JSONArray service;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tester);

        Parse.initialize(this, "mBDSSuZGGR18nA5KlgUtTbmb0LyvAFcMU0xTTEwS", "KowDwGn7b633Fvso9E2gC4odtucLzq4dLdurbtL7");

        testObj = new ParseObject("testObj");

        testServiceA = new JSONArray();
        testServiceA.put("Phone");
        try { testServiceA.put(19.99); }catch(JSONException e){ }
        testServiceA.put("Unlimited phone calls");

        testServiceB = new JSONArray();
        testServiceB.put("Test");
        try { testServiceB.put(9.99); }catch(JSONException e){ }
        testServiceB.put("Unlimited test msgs");

        testStoredOnDB();

        testAddToDB();

        testRemoveFromDB();
    }

    public void testStoredOnDB(){

        serviceArray.put(testServiceA);

        testObj.put("serviceArray", serviceArray);
        testObj.saveInBackground();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("testObj");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {

                if(e == null) {
                    serviceArray = parseObjects.get(0).getJSONArray("serviceArray");
                    if (serviceArray != null) {

                        try {
                            service = serviceArray.getJSONArray(0);
                        }catch(JSONException e2){ }

                        try {
                            name = service.getString(0);
                        } catch (JSONException e1) { }

                        if (name.equals("Phone")) {
                            System.out.println("Store service on DB test passed!");
                        } else {
                            System.out.println("No data in array. Fail!");
                        }
                    } else {
                        System.out.println("Nothing stored in DB. Fail!");
                    }
                }
                else{
                    System.out.println("Error accessing DB. Fail!");
                }
            }

        });
    }

    public void testAddToDB(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("testObj");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {

                if(e == null) {

                    serviceArray = parseObjects.get(0).getJSONArray("serviceArray");

                    if(serviceArray == null){
                        System.out.println("ARRAY IS NULL");

                    }
                }
                else{
                    System.out.println("Error accessing DB. Fail!");
                }

                serviceArray.put(testServiceB);

                System.out.println("REAL LENGTH: " + serviceArray.length());
                System.out.println(serviceArray.toString());

                parseObjects.get(0).put("serviceArray", serviceArray);
                parseObjects.get(0).saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e != null){
                            System.out.println("Save error");
                        }
                        ParseQuery<ParseObject> query2 = ParseQuery.getQuery("testObj");

                        query2.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> parseObjects, ParseException e) {
                                if(e == null){
                                    serviceArray = parseObjects.get(0).getJSONArray("serviceArray");

                                    System.out.println("ARRAY LENGTH: " + serviceArray.length());
                                    if(serviceArray.length() == 2){

                                        try {
                                            service = serviceArray.getJSONArray(1);
                                        }catch(JSONException e2){ }

                                        try {
                                            name = service.getString(0);
                                        } catch (JSONException e1) { }

                                        if(name.equals("Test")){
                                            System.out.println("Second service added successfully. Test passed!");
                                        }
                                        else{
                                            System.out.println(name);
                                            System.out.println("Second service incorrect");
                                        }
                                    }
                                    else{
                                        System.out.println("Incorrect number of services. Fail!");
                                        return;
                                    }

                                }else{
                                    System.out.println("Error accessing DB. Fail!");
                                }

                            }
                        });
                    }
                });


            }
        });


    }

    public void testRemoveFromDB(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("testObj");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {

                if(e == null) {

                    serviceArray = parseObjects.get(0).getJSONArray("serviceArray");

                    if(serviceArray == null){
                        System.out.println("ARRAY IS NULL");

                    }
                }
                else{
                    System.out.println("Error accessing DB. Fail!");
                }

                serviceArray.remove(1);

                System.out.println("REAL LENGTH: " + serviceArray.length());
                System.out.println(serviceArray.toString());

                parseObjects.get(0).put("serviceArray", serviceArray);
                parseObjects.get(0).saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e != null){
                            System.out.println("Save error");
                        }
                        ParseQuery<ParseObject> query2 = ParseQuery.getQuery("testObj");

                        query2.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> parseObjects, ParseException e) {
                                if(e == null){
                                    serviceArray = parseObjects.get(0).getJSONArray("serviceArray");

                                    System.out.println("ARRAY LENGTH: " + serviceArray.length());
                                    if(serviceArray.length() == 1){

                                        try {
                                            service = serviceArray.getJSONArray(0);
                                        }catch(JSONException e2){ }

                                        try {
                                            name = service.getString(0);
                                        } catch (JSONException e1) { }

                                        if(name.equals("Phone")){
                                            System.out.println("Second service removed successfully. Test passed!");
                                        }
                                        else{
                                            System.out.println("Incorrect service removed. Fail!");
                                        }
                                    }
                                    else{
                                        System.out.println("Incorrect number of services. Fail!");
                                        return;
                                    }

                                }else{
                                    System.out.println("Error accessing DB. Fail!");
                                }

                            }
                        });
                    }
                });


            }
        });


    }
}
