package com.team6.nick.telecorp;

import android.app.Activity;
import android.content.Intent;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Nick "bill" Deagon on 3/10/15.
 */
public abstract class BillEditor implements Action{

    static double bill;
    static double threshold;
    static boolean refresh = true;
    private static String username;
    private static String email;

    public static double getThreshold(final Activity a, String username) {
        //this.username = username;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                threshold = parseObjects.get(0).getDouble("Threshold");
                if(refresh == true) {
                    a.finish();
                    a.startActivity(a.getIntent());
                    refresh = false;
                }
            }
        });

        return threshold;
    }

    public static double getBill(final Activity a, String username) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                bill = parseObjects.get(0).getDouble("Bill");
                if(refresh == true) {
                    a.finish();
                    a.startActivity(a.getIntent());
                    refresh = false;
                }
            }
        });

        return bill;
    }

    public static void addToBill(final Activity a, String username, final double cost){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);


        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                double bill = parseObjects.get(0).getDouble("Bill");
                double threshold = parseObjects.get(0).getDouble("threshold");
                email = parseObjects.get(0).getString("email");
                bill += cost;

                parseObjects.get(0).put("Bill", bill);
                parseObjects.get(0).saveInBackground();

                ruleFactory rf = new ruleFactory();

                Properties prop = new Properties(a, bill, threshold, email);
                iRule1 billEmailRule = rf.makeRule("billEmailRule");
                billEmailRule.checkRule(prop);

            }
        });

    }

    public static void subFromBill(String username, final double cost){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                double bill = parseObjects.get(0).getDouble("Bill");
                bill -= cost;

                parseObjects.get(0).put("Bill", bill);
                parseObjects.get(0).saveInBackground();
            }
        });
    }


    @Override
    public void execute(Properties prop) {
        if (refresh = true){
            return;
        }
    }
}
