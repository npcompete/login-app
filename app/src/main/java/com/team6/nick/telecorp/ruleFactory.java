package com.team6.nick.telecorp;

/**
 * Created by nick on 3/11/15.
 */
public class ruleFactory {
    iRule1 makeRule(String s) {
        if(s.equals("billEmailRule")) {
            return new Rule1(new billAssessor(), new emailAction());
        }
        return null;
    }
}
