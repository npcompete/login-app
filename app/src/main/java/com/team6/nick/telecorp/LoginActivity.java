package com.team6.nick.telecorp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;

import com.parse.*;



public class LoginActivity extends ActionBarActivity {
    protected EditText usernameEmail;
    protected EditText usernamePassword;
    protected Button registerButton;
    protected Button logInButton;
    protected Button forgotPassword;
    protected int counter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // connects the user to the parse database using our specific credentials
        Parse.initialize(this, "mBDSSuZGGR18nA5KlgUtTbmb0LyvAFcMU0xTTEwS", "KowDwGn7b633Fvso9E2gC4odtucLzq4dLdurbtL7");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);


        // Initialize
        usernameEmail = (EditText)findViewById(R.id.username);
        usernamePassword = (EditText)findViewById(R.id.password);
        registerButton = (Button)findViewById(R.id.login);
        logInButton = (Button)findViewById(R.id.signup);
        forgotPassword = (Button)findViewById(R.id.forgotPass);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toLogIn = new Intent(LoginActivity.this, MainActivity.class);
                LoginActivity.this.startActivity(toLogIn);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = usernameEmail.getText().toString().trim();
                String password = usernamePassword.getText().toString().trim();

                ParseUser.logInInBackground(email, password, new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, com.parse.ParseException e) {
                        if(counter == 2)
                        {
                            registerButton.setClickable(false);
                            Toast.makeText(LoginActivity.this, "Account Locked. Too many attempts.", Toast.LENGTH_LONG).show();
                        }

                        if( e == null) {
                            ParseUser temp = ParseUser.getCurrentUser();
                            if(temp.getBoolean("isRep") == true || temp.getBoolean("isMarketRep") == true )
                            {
                                Toast.makeText(LoginActivity.this, "Welcome back!", Toast.LENGTH_LONG).show();

                                Intent takeUserHome = new Intent(LoginActivity.this, WelcomeRepresentative.class);
                                LoginActivity.this.startActivity(takeUserHome);
                            }
                            else {
                                Toast.makeText(LoginActivity.this, "Welcome back!", Toast.LENGTH_LONG).show();

                                Intent takeUserHome = new Intent(LoginActivity.this, Welcome.class);
                                LoginActivity.this.startActivity(takeUserHome);
                            }
                        }
                        else
                        {
                            counter++;
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage(e.getMessage());
                            builder.setTitle("Sorry!");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }

                });


            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toGetNewPassword = new Intent(LoginActivity.this, requestPassword.class);
                LoginActivity.this.startActivity(toGetNewPassword);
                //LoginActivity.this.finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}