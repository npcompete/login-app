package com.team6.nick.telecorp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Eric Lau on 3/9/2015.
 */
public class MailSenderActivity extends Activity{

    protected String email;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            email = extras.getString("email");
        }

      //  setContentView(R.layout.activity_email);

     //   Button addImage = (Button) findViewById(R.id.emailb);
    //    addImage.setOnClickListener(new View.OnClickListener() {
      //      public void onClick(View view) {
                final Mail m = new Mail("telecommteam6@gmail.com", "1t1sn0tEasy_");

                String[] toArr = {email};

                m.setTo(toArr);
                m.setFrom("telecommteam6@gmail.com");
                m.setSubject("Telecomm Bill Threshold");
                m.setBody("Hello, you have added a service/package which resulted in a bill higher than your threshold.");
                new AsyncTask<Void, Void, Void>() {
                    @Override public Void doInBackground(Void... arg) {
                        try
                        {
                            m.send();
                        }

                        catch(Exception e)
                        {
                            //Toast.makeText(MailApp.this, "There was a problem sending the email.", Toast.LENGTH_LONG).show();
                            Log.e("MailApp", "Could not send email", e);
                        }
                        return null;
                    }
                }.execute();
           // }
      //  });
    }
}
