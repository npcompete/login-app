package com.team6.nick.telecorp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;


public class viewAvailablePackages extends ActionBarActivity {

    protected Spinner packageViewSpinner;
    protected Button viewPackageButton;
    protected Button removePackageButton;
    protected Button addPackageButton;
    protected EditText packageName;
    protected EditText packageCost;
    protected TextView packageText;
    protected EditText packageDuration;

    protected JSONArray availablePackages = new JSONArray();

    JSONArray package1;
    String packageString = "";
    String printString = "";

    protected String name;
    protected double cost;
    protected String duration;
    private int indexToRemove;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Parse.initialize(this, "mBDSSuZGGR18nA5KlgUtTbmb0LyvAFcMU0xTTEwS", "KowDwGn7b633Fvso9E2gC4odtucLzq4dLdurbtL7");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_available_packages);

        packageViewSpinner = (Spinner) findViewById(R.id.packageSpinner);
        viewPackageButton = (Button) findViewById(R.id.viewPackButton);
        removePackageButton = (Button) findViewById(R.id.deletePackage);
        addPackageButton = (Button) findViewById(R.id.packButton);
        packageName = (EditText) findViewById(R.id.nameText2);
        packageCost = (EditText) findViewById(R.id.costText2);
        packageText = (TextView) findViewById(R.id.availPackText);
        packageDuration = (EditText) findViewById(R.id.durationText2);

        availablePackages = PackageMaker.getAvailablePackages(this);

        for (int j = 0; j < availablePackages.length(); j++) {

            try {
                package1 = availablePackages.getJSONArray(j);
            } catch (JSONException e1) {
            }

            packageString = "";

            // package description loop
            for (int i = 0; i < 3; i++) {
                packageString = packageString + " " + package1.optString(i);
            }

            printString = printString + "\n\n" + packageString;
        }
        packageText.setText(printString);


        String[] spinnerArray = new String[availablePackages.length()];

        for (int i = 0; i < availablePackages.length(); i++) {
            try {
                spinnerArray[i] = availablePackages.getJSONArray(i).getString(0);

            } catch (JSONException e1) {
            }
        }


        addPackageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = packageName.getText().toString().trim();

                String d = packageCost.getText().toString().trim();
                cost = Double.parseDouble(d);
                String t = packageDuration.getText().toString().trim();
                duration = t;

                PackageMaker.newPackage(name, cost, duration);
                finish();
                startActivity(getIntent());
            }
        });

        ArrayAdapter adapter = new ArrayAdapter(viewAvailablePackages.this, android.R.layout.simple_spinner_dropdown_item,
                spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        packageViewSpinner.setAdapter(adapter);

        viewPackageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(viewAvailablePackages.this, viewPackage.class);
                i.putExtra("packageIndex", packageViewSpinner.getSelectedItemPosition());
                viewAvailablePackages.this.startActivity(i); //
            }
        });

        removePackageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexToRemove = packageViewSpinner.getSelectedItemPosition();

                try{
                    package1 = availablePackages.getJSONArray(indexToRemove);
                }catch (Exception e) {}

                PackageMaker.removePackage(viewAvailablePackages.this, package1);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_available_packages, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
