package com.team6.nick.telecorp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;


public class WelcomeRepresentative extends ActionBarActivity {

    protected TextView username;
    public String[] userNames = {"no users"};
    protected Button viewUserButton;
    protected EditText changePassword;
    protected Button passwordButton;
    protected Button viewServices;
    protected Button viewPackages;
    protected TextView repType;
    protected Button logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Parse.initialize(this, "mBDSSuZGGR18nA5KlgUtTbmb0LyvAFcMU0xTTEwS", "KowDwGn7b633Fvso9E2gC4odtucLzq4dLdurbtL7");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_representative);

        viewServices = (Button)findViewById(R.id.viewServices);
        viewPackages = (Button)findViewById(R.id.packageButton);
        passwordButton = (Button)findViewById(R.id.changePassButton);
        changePassword = (EditText)findViewById(R.id.newPassword);
        final Spinner userSpinner = (Spinner) findViewById(R.id.viewUserSpinner);
        username = (TextView)findViewById(R.id.username);
        repType = (TextView)findViewById(R.id.RepType);
        logout = (Button)findViewById(R.id.logout);

        viewUserButton = (Button)findViewById(R.id.viewUsers);

        ParseUser temp = ParseUser.getCurrentUser();
        if(!temp.getBoolean("isMarketRep")) {
            viewServices.setVisibility(View.GONE);
            viewPackages.setVisibility(View.GONE);
            repType.setText("(Commercial)");
        }

        if(temp.getBoolean("isMarketRep")) {
            viewUserButton.setVisibility(View.GONE);
            userSpinner.setVisibility(View.GONE);
            repType.setText("(Marketing)");
        }

        username.setText(temp.getUsername());

        ParseQuery<ParseUser> query = ParseUser.getQuery();

        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    // The query was successful.
                    userNames = new String[objects.size()];

                    for(int i = 0; i < objects.size(); i++){
                        userNames[i] = objects.get(i).getUsername();
                    }

                    ArrayAdapter adapter = new ArrayAdapter(WelcomeRepresentative.this, android.R.layout.simple_spinner_dropdown_item,
                            userNames);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    userSpinner.setAdapter(adapter);
                } else {
                    // Something went wrong.
                }
            }
        });

        passwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPassword = changePassword.getText().toString().trim();

                ParseUser current = ParseUser.getCurrentUser();
                current.setPassword(newPassword);

                current.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(com.parse.ParseException e) {
                        if (e == null) {
                            Toast.makeText(WelcomeRepresentative.this, "Password Changed", Toast.LENGTH_LONG).show();

                            Intent takeToLogin = new Intent(WelcomeRepresentative.this, LoginActivity.class);
                            WelcomeRepresentative.this.startActivity(takeToLogin);
                            WelcomeRepresentative.this.finish();
                        } else {
                            Toast.makeText(WelcomeRepresentative.this, "Error changing password!", Toast.LENGTH_LONG).show();
                        }
                    }
                });


            }
        });

        viewServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomeRepresentative.this, ViewAvailableServices.class);
                startActivity(i);
            }
        });

        viewPackages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomeRepresentative.this, viewAvailablePackages.class);
                startActivity(i);
            }
        });

        viewUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomeRepresentative.this, viewUsers.class);
                i.putExtra("userName", (String)userSpinner.getSelectedItem());
                startActivity(i);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent i = new Intent(WelcomeRepresentative.this, LoginActivity.class);
                WelcomeRepresentative.this.startActivity(i);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome_representative, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
