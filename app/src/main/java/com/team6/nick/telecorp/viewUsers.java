package com.team6.nick.telecorp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;


public class viewUsers extends ActionBarActivity {

    String userName;
    public JSONArray serviceArray;
    public JSONArray packageArray;
    public String[] removeSpinnerArray = {"not loaded yet"};
    public String[] removePackageArray = {"not loaded yet"};
    // public String[] addPackageArray = {"not loaded yet"};

    JSONArray service;
    JSONArray package1;
    String serviceString = "";
    String printString = "";
    protected TextView serviceText;
    protected Button removeButton;
    protected Button addButton;
    protected Button packageAddButton;
    protected Button removePackageButton;
    private int indexToRemove;
    private int indexToAdd;
    private double tempBill;
    String username;

    private JSONArray availableServices = new JSONArray();
    private JSONArray availablePackages = new JSONArray();

    protected Spinner serviceSpinner;
    protected Spinner packageSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_users);

        final Spinner removeSpinner = (Spinner) findViewById(R.id.deleteSpinner);
        final Spinner removePackageSpinner = (Spinner)findViewById(R.id.deletePackageSpinner);
        //final Spinner addPackSpinner = (Spinner)findViewById(R.id.PackagesSpinner);
        removeButton = (Button)findViewById(R.id.deleteService);
        removePackageButton = (Button)findViewById(R.id.deletePackageButton);
        addButton = (Button)findViewById(R.id.addService);
        packageAddButton = (Button)findViewById(R.id.addPackage);
        packageSpinner = (Spinner) findViewById(R.id.PackagesSpinner);


        // adding some default services to test the drop-down menu
        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(final ParseObject parseObject, com.parse.ParseException e) {
                availableServices = parseObject.getJSONArray("availableServices");
                availablePackages = parseObject.getJSONArray("availablePackages");

                // Getting the spinner array to display the services.
                String[] spinnerArray = new String[availableServices.length()];

                for(int i = 0; i < availableServices.length(); i++){
                    try {
                        spinnerArray[i] = availableServices.getJSONArray(i).getString(0);

                    }catch(JSONException e1){ }
                }

                String[] spinnerArray2 = new String[availablePackages.length()];

                for(int i = 0; i < availablePackages.length(); i++){
                    try {
                        spinnerArray2[i] = availablePackages.getJSONArray(i).getString(0);

                    }catch(JSONException e1){ }
                }

                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    userName = extras.getString("userName");
                }

                ParseQuery<ParseObject> query1 = ParseQuery.getQuery("serviceRecords");
                query1.whereEqualTo("userName", userName);

                query1.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> parseObjects, ParseException e) {
                        if(e == null) {

                            serviceArray = parseObjects.get(0).getJSONArray("serviceArray");
                            packageArray = parseObjects.get(0).getJSONArray("packageArray");

                            removeSpinnerArray = new String[serviceArray.length()];
                            removePackageArray = new String[packageArray.length()];

                            for(int i = 0; i < serviceArray.length(); i++){
                                try {
                                    removeSpinnerArray[i] = serviceArray.getJSONArray(i).getString(0);

                                }catch(JSONException e1){ }
                            }

                            for(int i = 0; i < packageArray.length(); i++) {
                                try {
                                    removePackageArray[i] = packageArray.getJSONArray(i).getString(0);
                                } catch(JSONException e1) {}
                            }

                            ArrayAdapter adapter = new ArrayAdapter(viewUsers.this, android.R.layout.simple_spinner_dropdown_item,
                                    removeSpinnerArray);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            removeSpinner.setAdapter(adapter);

                            ArrayAdapter rmPackAdapter = new ArrayAdapter(viewUsers.this, android.R.layout.simple_spinner_dropdown_item,
                                    removePackageArray);
                            rmPackAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            removePackageSpinner.setAdapter(rmPackAdapter);


                            //services loop
                            for(int j = 0; j < serviceArray.length(); j++) {

                                try {
                                    service = serviceArray.getJSONArray(j);
                                }catch(JSONException e1){ }

                                serviceString = "";

                                // service description loop
                                for (int i = 0; i < 3; i++) {
                                    serviceString = serviceString + " " + service.optString(i);
                                }

                                printString = printString + "\n\n" + serviceString;
                            }

                            printString = printString + "\n";

                            //packages loop
                            for(int k = 0; k < packageArray.length(); k++)
                            {
                                try {
                                    package1 = packageArray.getJSONArray(k);
                                }catch(JSONException e1){ }

                                serviceString = "";

                                // service description loop
                                for (int i = 0; i < 2; i++) {
                                    serviceString = serviceString + " " + package1.optString(i);
                                }

                                printString = printString + "\n\n" + serviceString;
                            }
                            serviceText = (TextView)findViewById(R.id.serviceList);
                            serviceText.setText(printString);

                        } else{
                            Toast.makeText(viewUsers.this, "Error retrieving services", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                serviceSpinner = (Spinner) findViewById(R.id.addSpinner);

                ArrayAdapter adapter3 = new ArrayAdapter(viewUsers.this, android.R.layout.simple_spinner_dropdown_item,
                        spinnerArray);
                adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                serviceSpinner.setAdapter(adapter3);



                ArrayAdapter adapter2 = new ArrayAdapter(viewUsers.this, android.R.layout.simple_spinner_dropdown_item,
                        spinnerArray2);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                packageSpinner.setAdapter(adapter2);
            }
        });

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexToRemove = removeSpinner.getSelectedItemPosition();

                ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
                query.whereEqualTo("userName", userName);

                try{
                    service = serviceArray.getJSONArray((indexToRemove));
                }catch (Exception e) {}

                try {
                    tempBill = service.getDouble(1);
                } catch (Exception e) {
                }

                BillEditor.subFromBill(userName, tempBill);

                ServiceMaker.removeServiceFromUser(viewUsers.this, userName, service);
            }
        });

        removePackageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexToRemove = removePackageSpinner.getSelectedItemPosition();

                ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
                query.whereEqualTo("userName", userName);

                try {
                    package1 = availablePackages.getJSONArray(indexToRemove);
                } catch (Exception e) {
                }

                try {
                    tempBill = package1.getDouble(1);
                } catch (Exception e) {
                }

                BillEditor.subFromBill(userName, tempBill);

                PackageMaker.removePackageFromUser(viewUsers.this, userName, package1);
            }

        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexToAdd = serviceSpinner.getSelectedItemPosition();

                ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
                query.whereEqualTo("userName", userName);

                try{
                    service = availableServices.getJSONArray((indexToAdd));
                }catch (Exception e) {}

                try {
                    tempBill = service.getDouble(1);
                } catch (Exception e) {
                }

                BillEditor.addToBill(viewUsers.this, userName, tempBill);

                ServiceMaker.addServiceToUser(viewUsers.this, userName, service);

                Toast.makeText(viewUsers.this, "Service Added", Toast.LENGTH_LONG).show();
            }
        });

        packageAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                indexToAdd = packageSpinner.getSelectedItemPosition();

                ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
                query.whereEqualTo("userName", userName);

                try{
                    package1 = availablePackages.getJSONArray((indexToAdd));
                }catch (Exception e) {}

                try {
                    tempBill = package1.getDouble(1);
                } catch (Exception e) {
                }

                BillEditor.addToBill(viewUsers.this, userName, tempBill);

                PackageMaker.addPackageToUser(viewUsers.this, userName, package1);

                Toast.makeText(viewUsers.this, "Package Added", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_users, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
