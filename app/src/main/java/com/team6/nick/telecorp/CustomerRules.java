package com.team6.nick.telecorp;

import android.app.Activity;

/**
 * Created by Ana on 3/10/2015.
 */
public abstract class CustomerRules implements iRule1{
    protected int threshold;
    public abstract boolean assessBill(CustomerRules rules, String username, Activity a);
}
