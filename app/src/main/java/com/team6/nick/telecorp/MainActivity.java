package com.team6.nick.telecorp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;

import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONArray;

public class MainActivity extends ActionBarActivity {

    protected EditText usernameName;
    protected EditText usernameEmail;
    protected EditText usernamePassword;
    protected Button registerButton;
    protected EditText repAcc;
    protected CheckBox repAccBox;
    protected CheckBox marAccBox;
    protected CheckBox cusComeBox;

    protected String secret = new String("deagon"); // Password to create an account for company
    protected String secret1 = new String("luis");                           // reps

    public JSONArray serviceArray = new JSONArray();
    public JSONArray packageArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // remove this after first run
/*
        ParseObject available = new ParseObject("available");
        available.put("availablePackages",new JSONArray());
        available.put("availableServices",new JSONArray());
        available.saveInBackground();
*/


        //Initialize
        usernameName = (EditText)findViewById(R.id.nameRegisterEditText);
        usernameEmail = (EditText)findViewById(R.id.emailRegisterEditText);
        usernamePassword = (EditText)findViewById(R.id.passwordRegisterEditText);
        registerButton = (Button)findViewById(R.id.registerButton);

        repAcc = (EditText)findViewById(R.id.companyPassword);
        marAccBox = (CheckBox)findViewById(R.id.marRepCheckBox);
        repAccBox = (CheckBox)findViewById(R.id.cusRepCheckbox);
        cusComeBox = (CheckBox)findViewById(R.id.cusComCheckBox);

        repAcc.setVisibility(View.INVISIBLE);

        repAccBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((marAccBox.isChecked() || repAccBox.isChecked()) && ! ( marAccBox.isChecked() && repAccBox.isChecked()))
                {
                    repAcc.setVisibility(View.VISIBLE);
                }
                else
                {
                    repAcc.setVisibility(View.INVISIBLE);
                }
            }
        });
        marAccBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((marAccBox.isChecked() || repAccBox.isChecked()) && ! ( marAccBox.isChecked() && repAccBox.isChecked()))
                {
                    repAcc.setVisibility(View.VISIBLE);
                }
                else
                {
                    repAcc.setVisibility(View.INVISIBLE);
                }
            }
        });
        cusComeBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cusComeBox.isChecked())
                {
                    repAcc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    repAcc.setVisibility(View.INVISIBLE);
                }
            }
        });

        //listen to register button click
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = usernameName.getText().toString().trim();
                String password = usernamePassword.getText().toString().trim();
                String email = usernameEmail.getText().toString().trim();
                String repPassword = repAcc.getText().toString().trim();

                ParseUser user = new ParseUser();
                user.setUsername(username);
                user.setPassword(password);
                user.setEmail(email);

                if(repAccBox.isChecked())
                {
                    if(repPassword.equals(secret)) {
                        user.put("isRep", true);
                        user.put("isMarketRep", false);
                        user.put("isComCus", false);
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this, "Wrong Company Pass", Toast.LENGTH_LONG).show();
                        return;
                    }

                }
                else if(marAccBox.isChecked())
                {
                    if(repPassword.equals(secret1)) {
                        user.put("isMarketRep", true);
                        user.put("isRep", false);
                        user.put("isComCus", false);
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this, "Wrong Company Marketing Pass", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                else if(cusComeBox.isChecked())
                {
                    user.put("isRep", false);
                    user.put("isMarketRep", false);
                    user.put("isComCus", true);
                }
                else
                {
                    user.put("isRep", false);
                    user.put("isMarketRep", false);
                    user.put("isComCus", false);
                }

                ParseObject services = new ParseObject("serviceRecords"); // their services is an object, named by their username
                services.put("userName", username);
                services.put("serviceArray", serviceArray); // this ParseObject contains a list of their services
                services.put("packageArray", packageArray);
                services.put("Bill", 0.0);
                services.put("threshold", 25);
                services.put("email", email);
                services.saveInBackground();


                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(com.parse.ParseException e) {
                        if (e == null) {    // ...Toasty
                            Toast.makeText(MainActivity.this, "Account Created", Toast.LENGTH_LONG).show();

                            Intent takeToLogin = new Intent(MainActivity.this, LoginActivity.class);
                            MainActivity.this.startActivity(takeToLogin);
                            MainActivity.this.finish();
                        } else {
                            Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
                            // error registering
                        }
                    }
                });
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
