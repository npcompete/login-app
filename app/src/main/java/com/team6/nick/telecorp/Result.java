package com.team6.nick.telecorp;

/**
 * Created by Luis Arriaga on 3/11/2015.
 */
public class Result {
    String actionResults;
    String assessorResults;
    public String getActionResults() {
        return actionResults;
    }
    public void setActionResults(String actionResults) {
        this.actionResults = actionResults;
    }
    public String getAssessorResults() {
        return assessorResults;
    }
    public void setAssessorResults(String assessorResults) {
        this.assessorResults = assessorResults;
    }
    public boolean hasErrors() {
        return actionResults!=null || assessorResults!=null;
    }
}
