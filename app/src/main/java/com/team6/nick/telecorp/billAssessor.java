package com.team6.nick.telecorp;

/**
 * Created by nick on 3/11/15.
 */
public class billAssessor implements Assessor {

    public boolean evaluate(Properties prop){
        return prop.bill > prop.threshold;
    }

    public String getErrors() {
        return "";
    }
}
