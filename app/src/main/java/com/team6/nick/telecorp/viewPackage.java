package com.team6.nick.telecorp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;


public class viewPackage extends ActionBarActivity {

    protected TextView serviceView;

    protected Button serviceButton;
    protected Spinner serviceSpinner;

    protected int packageIndex;
    protected JSONArray package1;
    protected JSONArray service;
    protected JSONArray allServices;
    protected JSONArray availableServices;

    protected double bill;
    String serviceString = "";
    String printString = "";

    String username;

    private int indexToAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_package);

        serviceButton = (Button)findViewById(R.id.addServiceToPack);
        serviceSpinner = (Spinner)findViewById(R.id.serviceAddSpinner);

        serviceView = (TextView)findViewById(R.id.viewPackage);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            packageIndex = extras.getInt("packageIndex");
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                try {
                    package1 = parseObject.getJSONArray("availablePackages").getJSONArray(packageIndex);
                } catch (Exception e1) {
                }

                try {
                    allServices = package1.getJSONArray(3);
                } catch (JSONException e1) {
                }

                printString = "";


                for (int k = 0; k < allServices.length(); k++) {
                    serviceString = "";

                    try {
                        service = allServices.getJSONArray(k);
                    } catch (Exception e4) {}
                    // service description loop
                    for (int i = 0; i < 3; i++) {
                        try {
                            serviceString = serviceString + " " + service.getString(i);
                        } catch (Exception e5) {}
                    }

                    printString = printString + serviceString + "\n";
                    //   }

                }
                serviceView.setText(printString);

                ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
                query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, com.parse.ParseException e) {
                        availableServices = parseObject.getJSONArray("availableServices");

                        String[] spinnerArray = new String[availableServices.length()];

                        for(int i = 0; i < availableServices.length(); i++){
                            try {
                                spinnerArray[i] = availableServices.getJSONArray(i).getString(0);

                            }catch(JSONException e1){ }
                        }

                        serviceSpinner = (Spinner) findViewById(R.id.serviceAddSpinner);

                        ArrayAdapter adapter = new ArrayAdapter(viewPackage.this, android.R.layout.simple_spinner_dropdown_item,
                                spinnerArray);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        serviceSpinner.setAdapter(adapter);

                    }
                });

            }
        });

        serviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexToAdd = serviceSpinner.getSelectedItemPosition();

                try{
                    service = availableServices.getJSONArray((indexToAdd));
                }catch (Exception e) {}

                PackageMaker.addServiceToPackage(viewPackage.this, package1, service);

                Toast.makeText(viewPackage.this, "Service Added", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
