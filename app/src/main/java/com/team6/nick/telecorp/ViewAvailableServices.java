package com.team6.nick.telecorp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;


public class ViewAvailableServices extends ActionBarActivity {

    protected Button addServiceButton;
    protected Button removeServiceButton;
    protected EditText nameText;
    protected EditText costText;
    protected EditText descriptionText;
    protected TextView serviceText;

    String serviceString = "";
    String printString = "";
    JSONArray service;

    protected String name;
    protected double cost;
    protected String description;
    private int indexToRemove;

    protected JSONArray availableServices = new JSONArray();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_available_services);

        final Spinner removeSpinner = (Spinner) findViewById(R.id.availServs);

        serviceText = (TextView)findViewById(R.id.availServText);

        availableServices = ServiceMaker.getAvailableServices(ViewAvailableServices.this);

        String[] spinnerArray = new String[availableServices.length()];

        for(int i = 0; i < availableServices.length(); i++){
            try {
                spinnerArray[i] = availableServices.getJSONArray(i).getString(0);

            }catch(JSONException e1){ }
        }

        for(int j = 0; j < availableServices.length(); j++) {

            try {
                service = availableServices.getJSONArray(j);
            }catch(JSONException e1){ }

            serviceString = "";

            // service description loop
            for (int i = 0; i < 3; i++) {
                serviceString = serviceString + " " + service.optString(i);
            }

            printString = printString + "\n\n" + serviceString;
        }
        serviceText.setText(printString);


        addServiceButton = (Button)findViewById(R.id.button10);
        removeServiceButton = (Button)findViewById(R.id.remove);

        nameText = (EditText)findViewById(R.id.nameText);
        costText = (EditText)findViewById(R.id.costText);
        descriptionText = (EditText)findViewById(R.id.descriptionText);

        addServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameText.getText().toString().trim();

                String d = costText.getText().toString().trim();
                cost = Double.parseDouble(d);
                description = descriptionText.getText().toString().trim();

                ServiceMaker.newService(ViewAvailableServices.this, name, cost, description);
            }
        });



        ArrayAdapter adapter = new ArrayAdapter(ViewAvailableServices.this, android.R.layout.simple_spinner_dropdown_item,
                spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        removeSpinner.setAdapter(adapter);

        removeServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexToRemove = removeSpinner.getSelectedItemPosition();

                try{
                    service = availableServices.getJSONArray(indexToRemove);
                }catch (Exception e) {}

                ServiceMaker.removeService(ViewAvailableServices.this, service);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_available_services, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
