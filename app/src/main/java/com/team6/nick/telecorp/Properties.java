package com.team6.nick.telecorp;

import android.app.Activity;

import java.lang.reflect.Member;

/**
 * Created by Luis Arriaga on 3/10/2015.
 */
public class Properties {

    BillEditor billEditor;
    Activity a;
    double bill;
    double threshold;
    String email;

    public double getCurrentFee(final Activity a, String username) {
        return billEditor.getBill(a, username);
    }

    public Properties(Activity a, double bill, double threshold, String email) {
        this.a = a;
        this.bill = bill;
        this.threshold = threshold;
        this.email = email;
    }
}
