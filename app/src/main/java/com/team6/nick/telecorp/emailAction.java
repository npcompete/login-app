package com.team6.nick.telecorp;

import android.content.Intent;

/**
 * Created by nick on 3/11/15.
 */
public class emailAction implements Action {
    public void execute(Properties prop) {
        Intent i = new Intent(prop.a, MailSenderActivity.class);
        i.putExtra("email", prop.email);
        prop.a.startActivity(i);
    }
    public String getErrors() {
        return "";
    }
}
