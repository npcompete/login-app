package com.team6.nick.telecorp;

/**
 * Created by Luis Arriaga on 3/11/2015.
 */
public interface Assessor {
    public boolean evaluate(Properties prop);
    public String getErrors();

}
