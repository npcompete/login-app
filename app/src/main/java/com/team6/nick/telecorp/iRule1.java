package com.team6.nick.telecorp;

/**
 * Created by Luis Arriaga on 3/11/2015.
 */
public interface iRule1 {
    public boolean checkRule(Properties prop);
    public Result getResults();
}
