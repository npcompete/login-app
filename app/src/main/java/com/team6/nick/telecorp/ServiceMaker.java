package com.team6.nick.telecorp;

import android.app.Activity;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Ana on 2/24/2015.
 *
 * Created to create services and packages for JSON arrays. MEDIATOR class between the front-end
 * and the database.
 */
public class ServiceMaker {

    static JSONArray availableServices = new JSONArray();
    static boolean done;
    static boolean refresh = true;


    //takes a service JSONArray and adds it to a user
    public static void addServiceToUser(final Activity a, String username, JSONArray service)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);
        final JSONArray tempService = service;
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, com.parse.ParseException e) {
                if (e == null) {

                    JSONArray serviceArray = parseObjects.get(0).getJSONArray("serviceArray");
                    serviceArray.put(tempService);
                    parseObjects.get(0).put("serviceArray", serviceArray);
                    parseObjects.get(0).saveInBackground((new SaveCallback() {
                        @Override
                        public void done(com.parse.ParseException e) {
                            a.finish();
                            a.startActivity(a.getIntent());
                            refresh = true;
                        }
                    }));

                }
            }

        });
    }
    //removes a service from the user
    public static void removeServiceFromUser(final Activity a, String username, JSONArray service)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);
        final JSONArray tempService = service;
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, com.parse.ParseException e) {
                if (e == null) {
                    int indexToRemove = -1;
                    JSONArray serviceArray = parseObjects.get(0).getJSONArray("serviceArray");
                    for(int i = 0; i < serviceArray.length(); ++i)
                    {
                        String currentName = new String();
                        String tempName = new String();
                        try
                        {
                            currentName = serviceArray.getJSONArray(i).getString(0);
                            tempName = tempService.getString(0);
                        }
                        catch (JSONException e1) {}
                        if(currentName.equals(tempName)) {
                            indexToRemove = i;
                        }
                    }
                    try{
                        serviceArray.remove(indexToRemove);}
                    catch (Exception e2) {}
                    parseObjects.get(0).put("serviceArray", serviceArray);
                    parseObjects.get(0).saveInBackground((new SaveCallback() {
                        @Override
                        public void done(com.parse.ParseException e) {
                            a.finish();
                            a.startActivity(a.getIntent());
                        }
                    }));

                }
            }

        });
    }

    public static JSONArray service(String name, double cost, String info) {
        JSONArray tempService = new JSONArray();
        tempService.put(name);
        try {
            tempService.put(cost);
        } catch (JSONException e) {
        }
        tempService.put(info);
        return tempService;
    }

    //creates a service and adds it to the database of available services
    public static void newService(final Activity a, String name, double cost, String info)
    {
        final JSONArray tempService = service(name, cost, info);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                parseObject.getJSONArray("availableServices").put(tempService);

                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(com.parse.ParseException e) {
                        a.finish();
                        a.startActivity(a.getIntent());
                    }
                });
            }
        });
    }

    public static void removeService(final Activity a, final JSONArray service)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                JSONArray allServices = parseObject.getJSONArray("availableServices");
                int indexToRemove = -1;

                for(int i = 0; i < allServices.length(); ++i)
                {
                    JSONArray currentIteratedService = new JSONArray();
                    String servName = new String();
                    String curName = new String();

                    try {
                        currentIteratedService = allServices.getJSONArray(i);
                        curName = currentIteratedService.getString(0);
                        servName = service.getString(0);
                    }
                    catch (JSONException e1) {}
                    if(curName.equals(servName)) {
                        indexToRemove = i;
                    }
                }
                try {
                    allServices.remove(indexToRemove);
                }
                catch(Exception e2) {}
                parseObject.put("availableServices", allServices);
                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(com.parse.ParseException e) {
                        a.finish();
                        a.startActivity(a.getIntent());
                        refresh = true;
                    }
                });
            }
        });
    }

    public static JSONArray getAvailableServices(final Activity a)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
               availableServices = parseObject.getJSONArray("availableServices");
               if(ServiceMaker.refresh == true) {
                   a.finish();
                   a.startActivity(a.getIntent());
                   ServiceMaker.refresh = false;
               }
            }
        });

        return availableServices;
    }

    public static boolean isDone()
    {
        return done;
    }
}