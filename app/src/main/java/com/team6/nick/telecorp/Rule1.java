package com.team6.nick.telecorp;

import android.app.Activity;

/**
 * Created by Ana on 3/10/2015.
 */
public class Rule1 extends CustomerRules {
    private CustomerRules rules;
    private Assessor assessor;
    private Action action;
    private Result result;

    public Rule1(Assessor assessor, Action action) {
        this.assessor = assessor;
        this.action = action;
        this.result = new Result();
    }
    @Override
    public boolean assessBill(CustomerRules rules, String username, final Activity a) {
        return BillEditor.getBill(a, username) > threshold;
    }
    public Result getResults() {
        return result;
    }

    public boolean checkRule(Properties prop) {

        if(prop == null) System.exit(-1);

        if(assessor.evaluate(prop)) {
            action.execute(prop);
            result.setActionResults(action.getErrors());
            result.setAssessorResults(assessor.getErrors());
            return true;
        }
        result.setAssessorResults(assessor.getErrors());
        return false;
    }
}
