package com.team6.nick.telecorp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;


import com.parse.*;

public class requestPassword extends ActionBarActivity {
    protected EditText currentEmail;
    protected Button sendEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_password);

        currentEmail = (EditText)findViewById(R.id.email);
        sendEmail = (Button)findViewById(R.id.reset);

        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toLogIn = new Intent(requestPassword.this, LoginActivity.class);
                requestPassword.this.startActivity(toLogIn);
                String newEmail = currentEmail.getText().toString().trim();

                ParseUser.requestPasswordResetInBackground(newEmail, new RequestPasswordResetCallback() {
                    @Override
                    public void done(com.parse.ParseException e) {
                        if (e==null) {
                            Toast.makeText(requestPassword.this, "Email sent", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(requestPassword.this, "Invalid Email", Toast.LENGTH_LONG).show();

                        }
                    }
                });

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_request_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
