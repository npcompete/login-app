package com.team6.nick.telecorp;

import android.app.Activity;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

/**
 * Created by Ana on 2/24/2015. MEDIATOR class for linking packages to UI.
 */
public class PackageMaker {

    static JSONArray availablePackages = new JSONArray();
    static boolean refresh = true;


    public static JSONArray packageCreator(String name, double cost, String duration)
    {
        JSONArray packages = new JSONArray();
        JSONArray services = new JSONArray();

        packages.put(name);
        try {packages.put(cost);}
        catch (JSONException e) {}
        packages.put(duration);
        packages.put(services);

        return packages;
    }

    public static void newPackage(String name, double cost, String duration)
    {
        final JSONArray tempPackage = packageCreator(name, cost, duration);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                parseObject.getJSONArray("availablePackages").put(tempPackage);

                parseObject.saveInBackground();
            }
        });
    }
    public static void addServiceToPackage(final Activity a, JSONArray currentPackage, final JSONArray service)
    {
        final JSONArray tempPackage = currentPackage;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                JSONArray allPackages = parseObject.getJSONArray("availablePackages");
                int indexToAdd = -1;
                for(int i = 0; i < allPackages.length(); ++i)
                {
                    JSONArray currentIteratedPackage = new JSONArray();
                    String currentName = new String();
                    String tempName = new String();
                    try
                    {
                        currentIteratedPackage = allPackages.getJSONArray(i);
                        currentName = currentIteratedPackage.getString(0);
                        tempName = tempPackage.getString(0);
                    }
                    catch (JSONException e1) {}
                    if(currentName.equals(tempName))
                        indexToAdd = i;
                }
                try {allPackages.getJSONArray(indexToAdd).getJSONArray(3).put(service);}
                catch(JSONException e2) {}
                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        a.finish();
                        a.startActivity(a.getIntent());
                        refresh = true;
                    }
                });
            }
        });
    }

    public static void removeServiceFromPackage(JSONArray currentPackage, JSONArray service)
    {
        final JSONArray tempPackage = currentPackage;
        final JSONArray tempService = service;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                JSONArray allPackages = parseObject.getJSONArray("availablePackages");
                int indexToFind = -1;
                //iterating to find the right package
                for(int i = 0; i < allPackages.length(); ++i)
                {
                    JSONArray currentIteratedPackage = new JSONArray();
                    String currentName = new String();
                    String tempName = new String();
                    try
                    {
                        currentIteratedPackage = allPackages.getJSONArray(i);
                        currentName = currentIteratedPackage.getString(0);
                        tempName = tempPackage.getString(0);
                    }
                    catch (JSONException e1) {}
                    if(currentName.equals(tempName))
                        indexToFind = i;
                }
                JSONArray selectedPackage = new JSONArray();
                try {selectedPackage = allPackages.getJSONArray(indexToFind);}
                catch(JSONException e2) {}
                //iterating to remove the service
                int indexToRemove = -1;
                for(int i = 0; i < selectedPackage.length(); ++i)
                {
                    JSONArray currentIteratedService = new JSONArray();
                    String currentName = new String();
                    String tempName = new String();
                    try
                    {
                        currentIteratedService = selectedPackage.getJSONArray(i);
                        currentName = currentIteratedService.getString(0);
                        tempName = tempService.getString(0);
                    }
                    catch (JSONException e1) {}
                    if(currentName.equals(tempName))
                        indexToRemove = i;
                }
                try {selectedPackage.remove(indexToRemove);}
                catch(Exception e2) {}
                parseObject.saveInBackground();
            }
        });

    }

    public static void removePackage(final Activity a, JSONArray currentPackage)
    {
        final JSONArray tempPackage = currentPackage;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                JSONArray allPackages = parseObject.getJSONArray("availablePackages");
                int indexToRemove = -1;
                for(int i = 0; i < allPackages.length(); ++i)
                {
                    JSONArray currentIteratedPackage = new JSONArray();
                    String currentName = new String();
                    String tempName = new String();
                    try
                    {
                        currentIteratedPackage = allPackages.getJSONArray(i);
                        currentName = currentIteratedPackage.getString(0);
                        tempName = tempPackage.getString(0);
                    }
                    catch (JSONException e1) {}
                    if(currentName.equals(tempName))
                        indexToRemove = i;
                }
                try {allPackages.remove(indexToRemove);}
                catch(Exception e2) {}
                parseObject.put("availablePackages", allPackages);

                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        a.finish();
                        a.startActivity(a.getIntent());
                        refresh = true;
                    }
                });
            }
        });
    }

    public static void addPackageToUser(final Activity a, String username, JSONArray package1)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);
        final JSONArray tempPackage = package1 ;
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, com.parse.ParseException e) {
                if (e == null) {

                    JSONArray packageArray = parseObjects.get(0).getJSONArray("packageArray");
                    packageArray.put(tempPackage);
                    parseObjects.get(0).put("packageArray", packageArray);
                    parseObjects.get(0).saveInBackground((new SaveCallback() {
                        @Override
                        public void done(com.parse.ParseException e) {
                            a.finish();
                            a.startActivity(a.getIntent());
                        }
                    }));

                }
            }

        });
    }

    public static void removePackageFromUser(final Activity a, String username, JSONArray package1)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
        query.whereEqualTo("userName", username);
        final JSONArray tempPackage = package1;
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, com.parse.ParseException e) {
                if (e == null) {
                    int indexToRemove = -1;
                    JSONArray packageArray = parseObjects.get(0).getJSONArray("packageArray");
                    for(int i = 0; i < packageArray.length(); ++i)
                    {
                        JSONArray currentIteratedPackage = new JSONArray();
                        String currentName = new String();
                        String tempName = new String();
                        try
                        {
                            currentIteratedPackage = packageArray.getJSONArray(i);
                            currentName = currentIteratedPackage.getString(0);
                            tempName = tempPackage.getString(0);
                        }
                        catch (JSONException e1) {}
                        if(currentName.equals(tempName))
                            indexToRemove = i;
                    }

                    try{packageArray.remove(indexToRemove);}
                    catch (Exception e2) {}
                    parseObjects.get(0).put("packageArray", packageArray);
                    parseObjects.get(0).saveInBackground((new SaveCallback() {
                        @Override
                        public void done(com.parse.ParseException e) {
                            a.finish();
                            a.startActivity(a.getIntent());
                            refresh = true;
                        }
                    }));

                }
            }

        });
    }

    public static JSONArray getAvailablePackages(final Activity a)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                availablePackages = parseObject.getJSONArray("availablePackages");
                if(PackageMaker.refresh == true) {
                    a.finish();
                    a.startActivity(a.getIntent());
                    PackageMaker.refresh = false;
                }
            }
        });

        return availablePackages;
    }
}
