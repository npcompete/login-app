package com.team6.nick.telecorp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import java.text.DecimalFormat;
import java.util.List;

import com.parse.*;

import org.json.JSONArray;
import org.json.JSONException;


public class Welcome extends ActionBarActivity {
    protected EditText changePassword;
    protected Button passwordButton;
    protected Button addButton;
    protected Button viewButton;
    protected Button refresh;
    protected boolean done;
    protected double bill;
    protected double tempBill;
    protected TextView viewBill;
    protected TextView username;
    protected Button logout;
    protected Button thresholdButton;
    protected EditText setThreshold;

    protected TextView repType;
    protected double newThreshold;

    String userName;
    protected Spinner serviceSpinner;

    private int indexToAdd;

    private JSONArray availableServices = new JSONArray();

    private JSONArray service = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Parse.initialize(this, "mBDSSuZGGR18nA5KlgUtTbmb0LyvAFcMU0xTTEwS", "KowDwGn7b633Fvso9E2gC4odtucLzq4dLdurbtL7");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        passwordButton = (Button)findViewById(R.id.changePassButton);
        changePassword = (EditText)findViewById(R.id.password);
        addButton = (Button)findViewById(R.id.addButton);
        viewButton = (Button)findViewById(R.id.viewButton);
        viewBill = (TextView)findViewById(R.id.viewBill);
        username = (TextView)findViewById(R.id.userText);
        logout = (Button)findViewById(R.id.logout);
        refresh = (Button)findViewById(R.id.refresh);
        setThreshold = (EditText)findViewById(R.id.setThreshold);
        thresholdButton = (Button)findViewById(R.id.thresholdButton);
        repType = (TextView)findViewById(R.id.RepType);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                availableServices = parseObject.getJSONArray("availableServices");

                String[] spinnerArray = new String[availableServices.length()];

                for(int i = 0; i < availableServices.length(); i++){
                    try {
                        spinnerArray[i] = availableServices.getJSONArray(i).getString(0);

                    }catch(JSONException e1){ }
                }


                serviceSpinner = (Spinner) findViewById(R.id.viewSpinner);

                ArrayAdapter adapter = new ArrayAdapter(Welcome.this, android.R.layout.simple_spinner_dropdown_item,
                        spinnerArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                serviceSpinner.setAdapter(adapter);

            }
        });

        ParseUser current = ParseUser.getCurrentUser();
        if(current.getBoolean("isComCus")) {
            repType.setText("(Commercial)");
        }
        else
        {
            repType.setText("(Retail)");
        }
        username.setText(current.getUsername());

        bill = BillEditor.getBill(this, current.getUsername());

        DecimalFormat df = new DecimalFormat("#.##");
        bill = Double.valueOf(df.format(bill));
        viewBill.setText("$" + bill);

         refresh.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 finish();
                 startActivity(getIntent());
             }
         });

        thresholdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d = setThreshold.getText().toString().trim();
                ParseUser current = ParseUser.getCurrentUser();
                newThreshold = Double.parseDouble(d);

                ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
                query.whereEqualTo("userName", current.getUsername());

                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> parseObjects, ParseException e) {


                        parseObjects.get(0).put("threshold", newThreshold);
                        parseObjects.get(0).saveInBackground();
                    }
                });
                Toast.makeText(Welcome.this, "Threshold changed to " + newThreshold, Toast.LENGTH_LONG).show();
                finish();
                startActivity(getIntent());
            }
        });

         passwordButton.setOnClickListener(new View.OnClickListener()
          {
              @Override
              public void onClick(View v) {
                  String newPassword = changePassword.getText().toString().trim();

                  ParseUser current = ParseUser.getCurrentUser();
                  current.setPassword(newPassword);

                  current.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(com.parse.ParseException e) {
                          if (e == null) {
                              Toast.makeText(Welcome.this, "Password Changed", Toast.LENGTH_LONG).show();

                              Intent takeToLogin = new Intent(Welcome.this, LoginActivity.class);
                              Welcome.this.startActivity(takeToLogin);
                              Welcome.this.finish();
                          } else {
                              Toast.makeText(Welcome.this, "Error changing password!", Toast.LENGTH_LONG).show();
                          }
                      }
                  });
              }
          });

            addButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View v){
                ParseUser current = ParseUser.getCurrentUser();

                userName = current.getUsername();
                indexToAdd = serviceSpinner.getSelectedItemPosition();

                try {
                    service = availableServices.getJSONArray((indexToAdd));
                } catch (Exception e) {
                }

                try {
                    tempBill = service.getDouble(1);
                } catch (Exception e) {
                }

                BillEditor.addToBill(Welcome.this, userName, tempBill);

                ServiceMaker.addServiceToUser(Welcome.this, current.getUsername(), service);

                Toast.makeText(Welcome.this, "Service Added", Toast.LENGTH_LONG).show();
            }
            }
            );
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    Intent i = new Intent(Welcome.this, LoginActivity.class);
                    Welcome.this.startActivity(i);
                }
            });

            viewButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View v){
                Intent toView = new Intent(Welcome.this, ServiceView.class);
                Welcome.this.startActivity(toView);
            }
            }

            );
        }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}