package com.team6.nick.telecorp;

import android.app.Service;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;


public class ServiceView extends ActionBarActivity {

    protected TextView serviceText;
    public JSONArray serviceArray;
    public JSONArray packageArray;

    JSONArray service;
    JSONArray package1;

    String serviceString = "";
    String printString = "";
    public String[] spinnerArray = {"not loaded yet"};
    public String[] rmPackageSpinnerArray = {"not loaded yet"};
    protected Button removeButton;
    protected Button removePackage;
    protected Button packageAddButton;

    protected Spinner packageSpinner;

    protected double tempBill;
    protected double bill;

    String username;

    private int indexToRemove;
    private int indexToAdd;

    private JSONArray availablePackages = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_view);

        final Spinner removeSpinner = (Spinner) findViewById(R.id.removeSpinner);
        final Spinner removePackageSpinner = (Spinner) findViewById(R.id.removePackageSpinner);

        removeButton = (Button)findViewById(R.id.removeButton);
        removePackage = (Button)findViewById(R.id.removePackage);
        packageAddButton = (Button)findViewById(R.id.AddPackageButton);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("available");
        query.getInBackground("lzAV39bJCz", new GetCallback<ParseObject>() {
            @Override
            public void done(final ParseObject parseObject, com.parse.ParseException e) {

                availablePackages = parseObject.getJSONArray("availablePackages");

                String[] spinnerArray2 = new String[availablePackages.length()];

                for(int i = 0; i < availablePackages.length(); i++){
                    try {
                        spinnerArray2[i] = availablePackages.getJSONArray(i).getString(0);

                    }catch(JSONException e1){ }
                }

                ParseUser current = ParseUser.getCurrentUser();

                ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
                query.whereEqualTo("userName", current.getUsername());


                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> parseObjects, ParseException e) {
                        if (e == null) {

                            serviceArray = parseObjects.get(0).getJSONArray("serviceArray");
                            packageArray = parseObjects.get(0).getJSONArray("packageArray");

                            spinnerArray = new String[serviceArray.length()];
                            rmPackageSpinnerArray = new String[packageArray.length()];


                            for (int i = 0; i < serviceArray.length(); i++) {
                                try {
                                    spinnerArray[i] = serviceArray.getJSONArray(i).getString(0);

                                } catch (JSONException e1) {
                                }
                            }

                            for (int i = 0; i < packageArray.length(); i++) {
                                try {
                                    rmPackageSpinnerArray[i] = packageArray.getJSONArray(i).getString(0);
                                } catch (JSONException e2) {}
                            }

                            ArrayAdapter adapter = new ArrayAdapter(ServiceView.this, android.R.layout.simple_spinner_dropdown_item,
                                    spinnerArray);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            removeSpinner.setAdapter(adapter);

                            ArrayAdapter rmPackageAdapter = new ArrayAdapter(ServiceView.this, android.R.layout.simple_spinner_dropdown_item,
                                    rmPackageSpinnerArray);
                            rmPackageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            removePackageSpinner.setAdapter(rmPackageAdapter);


                            //services loop
                            for (int j = 0; j < serviceArray.length(); j++) {

                                try {
                                    service = serviceArray.getJSONArray(j);
                                } catch (JSONException e1) {
                                }

                                serviceString = "";

                                // service description loop
                                for (int i = 0; i < 3; i++) {
                                    serviceString = serviceString + " " + service.optString(i);
                                }

                                printString = printString + "\n\n" + serviceString;
                            }
                            printString = printString + "\n";

                            //packages loop
                            for (int k = 0; k < packageArray.length(); k++) {
                                try {
                                    package1 = packageArray.getJSONArray(k);
                                } catch (JSONException e1) {
                                }

                                serviceString = "";

                                // service description loop
                                for (int i = 0; i < 2; i++) {
                                    serviceString = serviceString + " " + package1.optString(i);
                                }

                                printString = printString + "\n\n" + serviceString;
                            }
                            serviceText = (TextView) findViewById(R.id.viewService);

                            serviceText.setText(printString);

                        } else {
                            Toast.makeText(ServiceView.this, "Error retrieving services", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                packageSpinner = (Spinner) findViewById(R.id.AddPackageSpinner);

                ArrayAdapter adapter2 = new ArrayAdapter(ServiceView.this, android.R.layout.simple_spinner_dropdown_item,
                        spinnerArray2);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                packageSpinner.setAdapter(adapter2);
            }
        });

        packageAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ParseUser current = ParseUser.getCurrentUser();

                username = current.getUsername();

                indexToAdd = packageSpinner.getSelectedItemPosition();

                ParseQuery<ParseObject> query = ParseQuery.getQuery("serviceRecords");
                query.whereEqualTo("userName", current.getUsername());

                try{
                    package1 = availablePackages.getJSONArray(indexToAdd);
                } catch (Exception e1){}

                bill = BillEditor.getBill(ServiceView.this, username);

                try {
                    tempBill = package1.getDouble(1);
                } catch (Exception e) {
                }

                PackageMaker.addPackageToUser(ServiceView.this, current.getUsername(), package1);

                BillEditor.addToBill(ServiceView.this, username, tempBill);

                Toast.makeText(ServiceView.this, "Package Added", Toast.LENGTH_LONG).show();


            }
        });


        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ParseUser current = ParseUser.getCurrentUser();
                username = current.getUsername();

                indexToRemove = removeSpinner.getSelectedItemPosition();

                try{
                    service = serviceArray.getJSONArray(indexToRemove);
                }catch (Exception e) {}

                try {
                    tempBill = service.getDouble(1);
                } catch (Exception e) {
                }

                tempBill -= 150;

                BillEditor.subFromBill(username, tempBill);

                ServiceMaker.removeServiceFromUser(ServiceView.this, current.getUsername(), service);

                Toast.makeText(ServiceView.this, "A $150.00 is charged for terminating the service", Toast.LENGTH_LONG).show();

            }
        });

        removePackage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ParseUser current = ParseUser.getCurrentUser();
                username = current.getUsername();
                indexToRemove = removePackageSpinner.getSelectedItemPosition();

                try {
                    package1 = packageArray.getJSONArray(indexToRemove);
                } catch (Exception e) {
                }

                try {
                    tempBill = service.getDouble(1);
                } catch (Exception e) {
                }

                tempBill -= 150;

                BillEditor.subFromBill(username, tempBill);

                PackageMaker.removePackageFromUser(ServiceView.this, current.getUsername(), package1);
                Toast.makeText(ServiceView.this, "A $150.00 is charged for terminating the package", Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_service_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
